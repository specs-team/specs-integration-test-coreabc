# Integration scenarios CoreABC

This family of scenarios integrates components that cover the SPECS flow up to the SLA monitoring phase (thus covering SLA negotiation, SLA implementation, SLA monitoring). More information about integration scenarios can be found in deliverables D1.5.1 and D1.5.2.

### Core-ABC1
This scenario integrates the Core-AB3 and Core-C2 scenarios. Involved artifacts enable the basic SLA negotiation (without ranking SLA Offers), SLA implementation, and SLA monitoring phases.

Details:
- Base Scenario ID: Core-AB3, Core-C2
- Added artefacts: /

Involved components:
- SLA Platform:	SLA Manager, Service Manager
- Negotiation module: SLO Manager, Supply Chain Manager
- Enforcement module: Planning, Implementation
- Monitoring module: Event Hub, MoniPoli Filter, Event Aggregator, SLOM Exporter, Event Archiver, CTP
- Vertical Layer: /
- Security mechanisms: /
- SPECS applications: /

### Core-ABC2
This scenario extends the Core-ABC1 scenario with the default SPECS Application. The involved artifacts enable the basic version of the SLA negotiation, SLA implementation, and SLA monitoring.

Details:
- Base Scenario ID: Core-ABC1
- Added artefacts: Default SPECS Application

Involved components:
- SLA Platform:	SLA Manager, Service Manager
- Negotiation module: SLO Manager, Supply Chain Manager
- Enforcement module: Planning, Implementation
- Monitoring module: Event Hub, MoniPoli Filter, Event Aggregator, SLOM Exporter, Event Archiver, CTP
- Vertical Layer: /
- Security mechanisms: /
- SPECS applications: Default SPECS Application