package eu.specs.project.integrationtest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.xml.sax.SAXException;

public class MonipoliClient {

	private MonitoringConfig configuration = new MonitoringConfig();
	
	public MonipoliClient(MonitoringConfig configuration) {
		this.configuration = configuration;
	}
	
	public boolean registerSlaDocument(String operation, String slaDocument) throws ClientProtocolException, IOException {
		
		String banner = "SPECS:coreC1:MonipoliClient: ";
		String mpURL = "http://" + configuration.getMonipoliIP() + ":" + configuration.getMonipoliPort()
		+ configuration.getMonipoliBaseURL();
		
		HttpClient mpClient = HttpClientBuilder.create().build();
		if (operation.equals("update")) {
			HttpPut request = new HttpPut(mpURL);
			request.addHeader("User-Agent", "SPECS Integration - Monitoring Core C");
			request.addHeader("Content-Type", "application/xml");
			HttpEntity slaEntity = new ByteArrayEntity(slaDocument.getBytes("UTF-8"));
			request.setEntity(slaEntity);
			HttpResponse response = mpClient.execute(request);
			System.out.println(banner + "HTTP_POST CODE: " 
	                + response.getStatusLine().getStatusCode());
			
			if (response.getStatusLine().getStatusCode() != 200) {
				System.out.println(banner + "ERROR: HTTP_POST CODE <> 200");
				return false;
			}
		}
		if (operation.equals("new")) {
			HttpPost request = new HttpPost(mpURL);
			request.addHeader("User-Agent", "SPECS Integration - Monitoring Core C");
			request.addHeader("Content-Type", "application/xml");
			HttpEntity slaEntity = new ByteArrayEntity(slaDocument.getBytes("UTF-8"));
			request.setEntity(slaEntity);
			HttpResponse response = mpClient.execute(request);
			System.out.println(banner + "HTTP_POST CODE: " 
	                + response.getStatusLine().getStatusCode());
			
			if (response.getStatusLine().getStatusCode() != 200) {
				System.out.println(banner + "ERROR: HTTP_POST CODE <> 200");
				return false;
			}
		}
		
		return true;
	}
	
	public boolean testRegisteredSecurityMetrics(String slaDocument) throws ClientProtocolException, IOException, XPathExpressionException, SAXException, ParserConfigurationException {
		String banner = "SPECS:coreC1:MonipoliClient:testRegisteredSecurityMetrics: ";
		
		ArrayList<MetricSLOMap> readSlaMetrics = getMetricsFromMonipoli();
		
		
		if (readSlaMetrics == null) {
			System.out.println(banner + "ERROR: HTTP_GET CODE: <>200");
		}
		
		SLA2SecurityMetrics ssm = new SLA2SecurityMetrics();
		ArrayList<MetricSLOMap> slaMetrics = ssm.getSecurityMetrics(slaDocument);
		
		
		/*
		if (readSlaMetrics.size() != slaMetrics.size()) {
			System.out.println(banner + "Number of SMs from SLA Document (" + slaMetrics.size() + ") not equal with SMs registered on Monipoli (" + readSlaMetrics.size() + ").");
			return false;
		}
		*/
		
		boolean check = true;
		int count = 0;
		int sla_count = 0;
		for (int i=0; i < slaMetrics.size(); i++) {
			count = 0;
			sla_count = 0;
			for (int j=0; j < readSlaMetrics.size(); j++) {
				if (slaMetrics.get(i).getSlaId().equals(readSlaMetrics.get(j).getSlaId())) {
					sla_count = 1;
					if (slaMetrics.get(i).getName().equals(readSlaMetrics.get(j).getName())) {
						count = 1;
						if (compareSecurityMetricsObjects(slaMetrics.get(i), readSlaMetrics.get(j)) != true) {
							check = false;
						}
					}
				}
			}
			if (count == 0 || sla_count == 0) {
				System.out.println(banner + "SMs from SLA Document mismatch with SMs from registered SLA on Monipoli");
				System.out.println(banner + "    -- " + slaMetrics.get(i).getName() + "." + slaMetrics.get(i).getSlaId());
				return false;
			}
		}
		return check;
	}

	public ArrayList<MetricSLOMap> getMetricsFromMonipoli() throws UnsupportedOperationException, IOException {
		String banner = "SPECS:coreC1:MonipoliClient:getSlaMetricsFromMonipoli: ";
		String mpURL = "http://" + configuration.getMonipoliIP() + ":" + configuration.getMonipoliPort()
		+ configuration.getMonipoliBaseURL();
		
		HttpClient mpClient = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(mpURL);
		request.addHeader("User-Agent", "SPECS Integration - Monitoring Core C");
		HttpResponse response = mpClient.execute(request);
		System.out.println(banner + "HTTP_GET CODE: " 
                + response.getStatusLine().getStatusCode());
		
		if (response.getStatusLine().getStatusCode() != 200) {
			return null;
		}
		
		BufferedReader rd = new BufferedReader(
				new InputStreamReader(response.getEntity().getContent()));
		
		ArrayList<String> mpRules = new ArrayList<String>();
		ArrayList<MetricSLOMap> readSlaMetrics = new ArrayList<MetricSLOMap>();
		
		String line = "";
		while ((line = rd.readLine()) != null) {
			mpRules.add(line);
		}
		
		for (int i=3; i < mpRules.size(); i+=7) {
			MetricSLOMap rsm = new MetricSLOMap();
			rsm.setName(mpRules.get(i+1).trim());
			rsm.setSlaId(mpRules.get(i+5).trim());
			rsm.setOperator(mpRules.get(i+4).trim());
			rsm.setOperand(mpRules.get(i+3).trim());
			rsm.setMetricReference(mpRules.get(i+2).trim());
			readSlaMetrics.add(rsm);
		}
		return readSlaMetrics;
	}
	
	private boolean compareSecurityMetricsObjects(MetricSLOMap src, MetricSLOMap dst) {
		
		String banner = "SPECS:coreC1:MonipoliClient:compareSecurityMetricsObjects: ";
		
		if (!src.getName().equals(dst.getName())) {
			System.out.println(banner + src.getName() + " <> " + dst.getName());
			return false;
		}
		if (!src.getMetricReference().equals(dst.getMetricReference())) {
			System.out.println(banner + src.getMetricReference() + " <> " + dst.getMetricReference());
			return false;
		}
		if (!src.getOperand().equals(dst.getOperand())) {
			System.out.println(banner + src.getOperand() + " <> " + dst.getOperand());
			return false;
		}
		if (!src.getOperator().equals(dst.getOperator())) {
			System.out.println(banner + src.getOperator() + " <> " + dst.getOperator());
			return false;
		}
		if (!src.getSlaId().equals(dst.getSlaId())) {
			System.out.println(banner + src.getSlaId() + " <> " + dst.getSlaId());
			return false;
		}
		return true;
		
	}
}