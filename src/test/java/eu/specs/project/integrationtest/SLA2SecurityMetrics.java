package eu.specs.project.integrationtest;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class SLA2SecurityMetrics {

	public ArrayList<MetricSLOMap> getSecurityMetrics(String slaDocument) throws SAXException, ParserConfigurationException, IOException, XPathExpressionException {
		HashMap<String,String> spVariables = new HashMap<String, String>();
		ArrayList<MetricSLOMap> metricSLOMap = new ArrayList<MetricSLOMap>();
		
		Document doc = stringToDocument(slaDocument);
		doc.getDocumentElement().normalize();	
		
		XPath xpath = XPathFactory.newInstance().newXPath();
		NodeList slaIdXml = (NodeList) xpath.compile("/AgreementOffer/Name").evaluate(doc, XPathConstants.NODESET);
		String slaId = slaIdXml.item(0).getFirstChild().getNodeValue();
		
		NodeList spList = (NodeList) xpath.compile("/AgreementOffer/Terms/All/ServiceProperties").evaluate(doc, XPathConstants.NODESET);
		for (int i=0; i < spList.getLength(); i++) {
			NodeList spEntry = spList.item(i).getChildNodes();
			for (int j=0; j < spEntry.getLength(); j++) {
				NodeList vsList = spEntry.item(j).getChildNodes();
				for (int k=0; k < vsList.getLength(); k++) {
					if (vsList.item(k).getNodeName() == "wsag:Variable") {
						spVariables.put(vsList.item(k).getAttributes().item(1).getNodeValue(), vsList.item(k).getAttributes().item(0).getNodeValue());
					}
				}
				
			}
		}
		
		NodeList gtList = (NodeList) xpath.compile("/AgreementOffer/Terms/All/GuaranteeTerm/ServiceLevelObjective/CustomServiceLevel/objectiveList").evaluate(doc, XPathConstants.NODESET);
		for (int i=0; i < gtList.getLength(); i++) {
			NodeList gtEntry = gtList.item(i).getChildNodes();
			for (int j=0; j < gtEntry.getLength(); j++) {
				if (gtEntry.item(j).getNodeName().equals("specs:SLO")) {
					MetricSLOMap sm = new MetricSLOMap(null, null, null, null, null, null);
					sm.setSlaId(slaId.trim());
					sm.setSloId(gtEntry.item(j).getAttributes().item(0).getNodeValue().trim());
					sm.setOperator(gtEntry.item(j).getChildNodes().item(3).getChildNodes().item(1).getChildNodes().item(1).getFirstChild().getNodeValue().trim());
					sm.setOperand(gtEntry.item(j).getChildNodes().item(3).getChildNodes().item(1).getChildNodes().item(3).getFirstChild().getNodeValue().trim());
					sm.setName(gtEntry.item(j).getChildNodes().item(1).getFirstChild().getNodeValue().trim());
					sm.setMetricReference(spVariables.get(sm.getName().trim()));
					metricSLOMap.add(sm);
				}
			}
		}
		return metricSLOMap;
		
	}
 
	private Document stringToDocument(final String xmlSource)   
		    throws SAXException, ParserConfigurationException, IOException {  
		    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
		    DocumentBuilder builder = factory.newDocumentBuilder();  
		    return builder.parse(new InputSource(new StringReader(xmlSource)));  
		}  
}