package eu.specs.project.integrationtest;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.xml.sax.SAXException;

public class TestIntegration {

	public static void main(String[] args) {
		
		MonitoringConfig config = new MonitoringConfig();
		String machineIp = "194.102.62.232";
		config.setMonipoliIP(machineIp);
		config.setEventHubIP(machineIp);
		config.setEventArchiverIP(machineIp);
		config.setCtpIP(machineIp);
		String bannerTi = "SPECS:coreC1:TestIntegration: ";
		String slaDocument = null;
		try {
			slaDocument = new SLA2String(System.getProperty("user.dir") + "/src/test/resources/sla-test.xml").getSlaDocument();
		} catch (IOException e) {
			System.out.println("--------------------------");
			e.printStackTrace();
			System.out.println("--------------------------");
			System.out.println(bannerTi + "EXCEPTION!");
			System.out.println("--------------------------");
			System.exit(1);
		}
		
		// ++++++++++++ MonitPoli ++++++++++++++
		
		String bannerMp = "SPECS:coreC1:TestMonipoli: ";
		MonipoliClient mp = new MonipoliClient(config);
		try {
			if (mp.registerSlaDocument("new", slaDocument.toString())) {
				if(!mp.testRegisteredSecurityMetrics(slaDocument.toString())) {
					System.out.println("--------------------------------------");
					System.out.println(bannerMp + "All tests: FAILED!");
					System.out.println("--------------------------------------");
				} else {
					System.out.println("--------------------------------------");
					System.out.println(bannerMp + "All tests: Test PASSED!");
					System.out.println("--------------------------------------");
				}
			}
		} catch (XPathExpressionException | IOException | SAXException | ParserConfigurationException e) {
			e.printStackTrace();
			System.out.println("--------------------------");
			System.out.println(bannerMp + "EXCEPTION!");
			System.out.println("--------------------------");
			System.exit(1);
		}
		
		// ++++++++++++ Event Hub ++++++++++++++
		
		String bannerEh = "SPECS:coreC1:TestEventHub: ";
		MonitoringEventGenerator ev = new MonitoringEventGenerator(slaDocument);
		EventHubClient eh = new EventHubClient(config);
		MonitoringEvent event = null;
		try {
			event = ev.generateRandom(false);
		} catch (XPathExpressionException | SAXException | ParserConfigurationException | IOException e) {
			e.printStackTrace();
			System.out.println("--------------------------");
			System.out.println(bannerEh + "EXCEPTION!");
			System.out.println("--------------------------");
			System.exit(1);
		}
		String _ej = event.getEventAsJSON(event);
		if (_ej != null) {
			System.out.println("--------------------------");
			System.out.println(bannerEh + "Event generated:");
			System.out.println("--------------------------");
			System.out.println(_ej);
			System.out.println("--------------------------");
		} else {
			System.out.println("--------------------------");
			System.out.println(bannerEh + "Event generator failed!");
			System.out.println("--------------------------");
		}
		
		try {
			if (eh.publishEvent(event)) {
				System.out.println("--------------------------");
				System.out.println(bannerEh + "All tests PASSED!");
				System.out.println("--------------------------");
			} else {
				System.out.println("--------------------------");
				System.out.println(bannerEh + "Event not published!");
				System.out.println("--------------------------");
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("--------------------------");
			System.out.println(bannerEh + "EXCEPTION!");
			System.out.println("--------------------------");
			System.exit(1);
		}
		
		// ++++++++++++ Event Archiver ++++++++++++++
		String bannerEa = "SPECS:coreC1:TestEventArchiver: ";
		EventArchiverClient ea = new EventArchiverClient(config);
		try {
			if(ea.verifyPublishedEvent(event)) {
				System.out.println("--------------------------");
				System.out.println(bannerEa + "All tests PASSED!");
				System.out.println("--------------------------");
			} else {
				System.out.println("--------------------------");
				System.out.println(bannerEa + "All tests FAILED!");
				System.out.println("--------------------------");
			}
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
			System.out.println("--------------------------");
			System.out.println(bannerEa + "EXCEPTION!");
			System.out.println("--------------------------");
			System.exit(1);
		}
		
		// ++++++++++++ Event Archiver ++++++++++++++
		String bannerCtp = "SPECS:coreC2:TestCTP: ";
		CtpClient ctp = new CtpClient(config);
		System.out.println(bannerCtp + "SLA ID: " + event.getLabels().get(0));
		System.out.println(bannerCtp + "Metric Name: " + event.getLabels().get(1));
		try {
			if(ctp.verifyRegisteredSla(event.getLabels().get(0))) {
				System.out.println("--------------------------");
				System.out.println(bannerCtp + "SLA registration test PASSED!");
				System.out.println("--------------------------");
			} else {
				System.out.println("--------------------------");
				System.out.println(bannerCtp + "SLA registration test FAILED!");
				System.out.println("--------------------------");
			}
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
			System.out.println("--------------------------");
			System.out.println(bannerCtp + "EXCEPTION!");
			System.out.println("--------------------------");
			//System.exit(1);
		}
		try {
			if(ctp.verifyRegisteredMetric(event.getLabels().get(1))) {
				System.out.println("--------------------------");
				System.out.println(bannerCtp + "Metric registration test PASSED!");
				System.out.println("--------------------------");
			} else {
				System.out.println("--------------------------");
				System.out.println(bannerCtp + "Metric registration test FAILED!");
				System.out.println("--------------------------");
			}
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
			System.out.println("--------------------------");
			System.out.println(bannerCtp + "EXCEPTION!");
			System.out.println("--------------------------");
			System.exit(1);
		}
		
	}

}
