package eu.specs.project.integrationtest;

public class MonitoringConfig {
	
	private String eventHubIP = "127.0.0.1";
	private String eventHubPort = "9090";
	private String eventHubBaseURL = "/events/integration";
	
	private String eventArchiverIP = "127.0.0.1";
	private String eventArchiverPort = "10101";
	private String eventArchiverBaseURL = "/monitoring/events";
	
	private String monipoliIP = "127.0.0.1";
	private String monipoliPort = "5000";
	private String monipoliBaseURL = "/monipoli";
	
	private String ctpIP = "127.0.0.1";
	private String ctpPort = "7070";
	private String ctpBaseURL = "/api/1.0";
	private String ctpSlaViewURL = "/serviceViews";
	private String ctpMetricViewURL = "/metrics";
	
	public String getEventHubIP() {
		return eventHubIP;
	}
	public void setEventHubIP(String eventHubIP) {
		this.eventHubIP = eventHubIP;
	}
	public String getEventHubPort() {
		return eventHubPort;
	}
	public void setEventHubPort(String eventHubPort) {
		this.eventHubPort = eventHubPort;
	}
	public String getEventHubBaseURL() {
		return eventHubBaseURL;
	}
	public void setEventHubBaseURL(String eventHubBaseURL) {
		this.eventHubBaseURL = eventHubBaseURL;
	}
	public String getEventArchiverIP() {
		return eventArchiverIP;
	}
	public void setEventArchiverIP(String eventArchiverIP) {
		this.eventArchiverIP = eventArchiverIP;
	}
	public String getEventArchiverPort() {
		return eventArchiverPort;
	}
	public void setEventArchiverPort(String eventArchiverPort) {
		this.eventArchiverPort = eventArchiverPort;
	}
	public String getEventArchiverBaseURL() {
		return eventArchiverBaseURL;
	}
	public void setEventArchiverBaseURL(String eventArchiverBaseURL) {
		this.eventArchiverBaseURL = eventArchiverBaseURL;
	}
	public String getMonipoliIP() {
		return monipoliIP;
	}
	public void setMonipoliIP(String monipoliIP) {
		this.monipoliIP = monipoliIP;
	}
	public String getMonipoliPort() {
		return monipoliPort;
	}
	public void setMonipoliPort(String monipoliPort) {
		this.monipoliPort = monipoliPort;
	}
	public String getMonipoliBaseURL() {
		return monipoliBaseURL;
	}
	public void setMonipoliBaseURL(String monipoliBaseURL) {
		this.monipoliBaseURL = monipoliBaseURL;
	}
	public String getCtpIP() {
		return ctpIP;
	}
	public void setCtpIP(String ctpIP) {
		this.ctpIP = ctpIP;
	}
	public String getCtpPort() {
		return ctpPort;
	}
	public void setCtpPort(String ctpPort) {
		this.ctpPort = ctpPort;
	}
	public String getCtpBaseURL() {
		return ctpBaseURL;
	}
	public void setCtpBaseURL(String ctpBaseURL) {
		this.ctpBaseURL = ctpBaseURL;
	}	
	public String getCtpSlaViewURL() {
		return ctpSlaViewURL;
	}
	public void setCtpSlaViewURL(String ctpSlaViewURL) {
		this.ctpSlaViewURL = ctpSlaViewURL;
	}
	public String getCtpMetricViewURL() {
		return ctpMetricViewURL;
	}
	public void setCtpMetricViewURL(String ctpMetricViewURL) {
		this.ctpMetricViewURL = ctpMetricViewURL;
	}
}
