package eu.specs.project.integrationtest;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class SLA2String {
	
	private String slaDocumentPath;
	
	public  SLA2String(String slaDocumentPath) {
		this.slaDocumentPath = slaDocumentPath;
	}
	
	public String getSlaDocument() throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(new File(this.slaDocumentPath)));
		String line;
		StringBuilder slaDocument = new StringBuilder();
		while((line=br.readLine())!= null){
		   slaDocument.append(line);
		}
		br.close();
		return slaDocument.toString();
	}
	
}
