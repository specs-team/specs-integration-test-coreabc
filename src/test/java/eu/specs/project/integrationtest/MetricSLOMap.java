package eu.specs.project.integrationtest;

public class MetricSLOMap {
	private String slaId;
	private String sloId;
	private String metricReference;
	private String operator;
	private String operand;
	private String name;
	
	public MetricSLOMap (String slaId, String sloId, String metricReference, String operator, String operand, String name) {
		this.slaId = slaId;
		this.sloId = sloId;
		this.metricReference = metricReference;
		this.operator = operator;
		this.operand = operand;
		this.name = name;
	}
	
	public MetricSLOMap () {
		this.slaId = "";
		this.sloId = "";
		this.metricReference = "";
		this.operator = "";
		this.operand = "";
		this.name = "";
	}
	
	public String getSlaId() {
		return slaId;
	}
	public void setSlaId(String slaId) {
		this.slaId = slaId;
	}
	public String getSloId() {
		return sloId;
	}
	public void setSloId(String sloId) {
		this.sloId = sloId;
	}
	public String getMetricReference() {
		return metricReference;
	}
	public void setMetricReference(String metricReference) {
		this.metricReference = metricReference;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getOperand() {
		return operand;
	}
	public void setOperand(String operand) {
		this.operand = operand;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}